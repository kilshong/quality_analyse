package com.capgemini.demo.controller.LoginController;

import cn.dev33.satoken.stp.StpUtil;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "退出逻辑")
@RestController
public class LogoutController {
    @PostMapping("/api/logout")
    @ApiOperation(value = "退出登录")
    public RestResponse logout(){
        try {
            StpUtil.checkLogin();
            StpUtil.login(StpUtil.getLoginId());
            return RestResponse.succuess("退出成功！！！");
        }catch (Exception e){
            e.printStackTrace();
            return RestResponse.fail("未登录账号，无法退出");
        }
    }
}
