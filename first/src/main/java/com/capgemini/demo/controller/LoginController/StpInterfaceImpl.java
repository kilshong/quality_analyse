package com.capgemini.demo.controller.LoginController;

import cn.dev33.satoken.stp.StpInterface;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.capgemini.demo.entity.login.UserRoleRelEntity;
import com.capgemini.demo.entity.permission.RoleEntity;
import com.capgemini.demo.mapper.login.RoleMapper;
import com.capgemini.demo.mapper.login.UserRoleMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class StpInterfaceImpl implements StpInterface {
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    UserRoleMapper userRoleMapper;

    @Override
    public List<String> getPermissionList(Object loginId, String loginKey) {
      return null;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginKey) {
        List<String> list = new ArrayList<String>();
        UserRoleRelEntity user_id = userRoleMapper.selectOne(new QueryWrapper<UserRoleRelEntity>().eq("USER_ID", loginId.toString()));
        RoleEntity roleEntity = roleMapper.selectOne(new QueryWrapper<RoleEntity>().eq("ROLE_ID", user_id.getRoleId()));
        String roleName = roleEntity.getRoleName();
        list.add(roleName);
        return list;
    }

}
