package com.capgemini.demo.controller.BitBucketController;

<<<<<<< HEAD
import com.capgemini.demo.entity.bitbucket.BitBucketEntity;
import com.capgemini.demo.mapper.BitBucketMapper.BitBucketMapper;
import com.capgemini.demo.mapper.BitBucketMapper.BitMessageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
=======
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.capgemini.demo.entity.bitbucket.BitBucketEntity;
import com.capgemini.demo.mapper.BitBucketMapper.BitBucketMapper;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
>>>>>>> e45ce78054dac82f08523f7d91b98cbb6852a0d5
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

<<<<<<< HEAD
import java.util.List;
=======
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
>>>>>>> e45ce78054dac82f08523f7d91b98cbb6852a0d5

@Api(tags = "信息Controller")
@RestController
@RequestMapping("/api/message")
public class MessageController {
    @Autowired
    private BitMessageMapper bitMessageMapper;


    @ApiOperation(value = "用动态sql，通过某字段查询出信息")
    @RequestMapping(value = "/selectAll",method = RequestMethod.GET)
    public List<BitBucketEntity> selectAll(@Param("REPOSITORY_NAME") String REPOSITORY_NAME,
                                           @Param("REPOSITORY_FULL_NAME") String REPOSITORY_FULL_NAME,
                                           @Param("AUTHOR_RAW") String AUTHOR_RAW,
                                           @Param("BITBUCKET_TYPE") String BITBUCKET_TYPE,
                                           @Param("OP_TIME") String OP_TIME){
        List<BitBucketEntity> list = bitMessageMapper.selectAll(REPOSITORY_NAME,REPOSITORY_FULL_NAME,AUTHOR_RAW,BITBUCKET_TYPE,OP_TIME);
        System.out.println(list);
        return list;
    }

<<<<<<< HEAD
    @ApiOperation(value = "查询名字提交的次数")
    @RequestMapping(value = "/count",method = RequestMethod.GET)
    public int count(@RequestParam("AUTHOR_RAW") String AUTHOR_RAW,
                     @RequestParam("MESSAGE") String MESSAGE){
        return bitMessageMapper.count(AUTHOR_RAW,MESSAGE);
    }
=======
    @Autowired
    private BitBucketMapper bitBucketMapper;

    @GetMapping("getAll/{name}")
    public RestResponse getAll(@PathVariable String name){
        QueryWrapper<BitBucketEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("REPOSITORY_NAME", name);
        List<BitBucketEntity> bitBucketEntities = bitBucketMapper.selectList(wrapper);
        HashMap<String, Object> map = new HashMap<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (bitBucketEntities!=null){
            Map<String, List<BitBucketEntity>> groupList = bitBucketEntities.stream().collect(Collectors.groupingBy(BitBucketEntity::getAuthorRaw));
            Set<Map.Entry<String, List<BitBucketEntity>>> set = groupList.entrySet();
            for (Map.Entry<String, List<BitBucketEntity>> entry : set) {
                List<BitBucketEntity> value = entry.getValue();
                List<BitBucketEntity> collect = value.stream().sorted(Comparator.comparing(BitBucketEntity::getOptrationTime)).collect(Collectors.toList());
                map.put(entry.getKey(), collect);
            }
            return  RestResponse.succuess(map);
        }
        return RestResponse.fail("查询失败");
    }

>>>>>>> e45ce78054dac82f08523f7d91b98cbb6852a0d5
}
