package com.capgemini.demo.controller.LoginController;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.capgemini.demo.dto.login.LoginDto;
import com.capgemini.demo.entity.permission.UserEntity;
import com.capgemini.demo.mapper.login.LoginIdMapper;
import com.capgemini.demo.mapper.login.LoginMapper;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Api(tags = "登录Controller")
@Slf4j
@RestController
@RequestMapping(value = "/api")
public class LoginController {
    @Autowired
    LoginMapper loginMapper;
    @Autowired
    LoginIdMapper loginIdMapper;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    private RestResponse loginIn(LoginDto loginDto, HttpServletRequest request) {
        String username = loginDto.getUsername();
        String password = loginDto.getPassword();
        if (username != null && password != null) {
            UserEntity user = loginMapper.selectOne(new QueryWrapper<UserEntity>().eq("USER_NAME", username));
            String passwordFormDB = user.getPassword();
            if (passwordFormDB.equals(password)) {
                String uId = user.getUId();
                StpUtil.login(uId.toString());
                return RestResponse.succuess("登录成功！！！");
            }

        } else {
            return RestResponse.fail("用户名或密码为空，请重新输入！！！");
        }
        return RestResponse.succuess("服务器内部异常！！！");
    }

}
