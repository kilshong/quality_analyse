package com.capgemini.demo.controller.BitBucketController;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.capgemini.demo.dto.commit.GetCommitsDto;
import com.capgemini.demo.entity.bitbucket.BitBucketEntity;
import com.capgemini.demo.mapper.BitBucketMapper.BitBucketMapper;
import com.capgemini.demo.service.GetCommitsService;
import com.capgemini.demo.tools.JsonUtils.JsonUtils;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "bitbucket获取接口信息")
@Slf4j
@RequestMapping(value = "/api/commit")
@RestController
public class GetCommitController {
    @Autowired
    GetCommitsService getCommitsService;
    @Autowired
    BitBucketMapper bitBucketMapper;

    /**
     * 更新提交的信息
     *
     * @param getCommitsDto
     * @return
     */

    @ApiOperation(value = "从BitBucketAPI获取数据存入数据库")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public RestResponse allCommit(GetCommitsDto getCommitsDto) {
        String user = getCommitsDto.getUser();
        String repository = getCommitsDto.getRepository();
        String api = "/2.0/repositories/" + user + "/" + repository + "/commits?until=main";
        JSONObject jsonObject = JsonUtils.loadJson(api);
        boolean b = getCommitsService.addCommits(jsonObject);
        if (!b) {
            return RestResponse.succuess("用户更新成功！！！");
        }
        log.debug(jsonObject.toJSONString());
        return RestResponse.succuess(jsonObject);
    }

    @ApiOperation(value = "从数据库中某人在某库获取提交总数量")
    @RequestMapping(value = "/getcommit", method = RequestMethod.GET)
    public RestResponse getCommit(@RequestParam("author") String author, @RequestParam("repositoryname") String repositoryName) {
        int numCommits = getCommitsService.getNumCommits(author, repositoryName);
        if (numCommits == 0) {
            return RestResponse.succuess("该用户没有提交任何信息");
        }
        return RestResponse.succuess(author + "用户提交了" + numCommits);
    }

    @ApiOperation(value = "从数据库中查看所有的commit的相信想那些")
    @RequestMapping(value = "/commitsMessage", method = RequestMethod.GET)
    public List<BitBucketEntity> commitsMessage(
            @RequestParam(value = "author") String author,
            @RequestParam(value = "repositoryname") String repositoryName
    ) {
        System.out.println("tokenCommite"+StpUtil.getTokenInfo().getTokenValue());
        // 如果用户为空查找所有的信息
        List<BitBucketEntity> bitBucketEntities = bitBucketMapper.selectList(new QueryWrapper<BitBucketEntity>().and(i -> i.like("AUTHOR_RAW", author + "% ").eq("REPOSITORY_NAME", repositoryName)));
        return bitBucketEntities;
    }


}
