package com.capgemini.demo.controller.UserController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.capgemini.demo.entity.permission.RoleEntity;
import com.capgemini.demo.entity.permission.RoleMenuRelEntity;
import com.capgemini.demo.entity.permission.UserEntity;
import com.capgemini.demo.entity.permission.UserRoleRelEntity;
import com.capgemini.demo.mapper.login.LoginMapper;
import com.capgemini.demo.mapper.login.RoleMapper;
import com.capgemini.demo.mapper.rolemenurel.RoleMenuRelMapper;
import com.capgemini.demo.mapper.userrolerel.UserRoleRelMapper;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Api("用户管理模块Controller")
@Slf4j
@RequestMapping("/api/user")
@RestController
public class UserController {
    /**
     * 用户管理 ----- 账号管理
     */

    @Autowired
    private LoginMapper loginMapper;

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleMenuRelMapper roleMenuRelMapper;
    @Autowired
    private UserRoleRelMapper userRoleRelMapper;
    // @RequestMapping(value = "/people/addUser",method = RequestMethod.POST)
    @ApiOperation("添加用户接口")
    @PostMapping("/people/addUser")
    public RestResponse addUser(UserEntity user){
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("USER_NAME", user.getUsername());
        Integer integer = loginMapper.selectCount(wrapper);
        if (integer==0){
            int insert = loginMapper.insert(user);
            if (insert>0){
                return RestResponse.succuess("用户添加成功");
            }
        }else {
            return RestResponse.fail("用户已经存在");
        }
       return RestResponse.fail("添加失败");
    }
    // @RequestMapping(value = "/people/deleteUser",method = RequestMethod.POST)
    @ApiOperation("删除用户接口")
    @PostMapping("/people/deleteUser/{username}")
    public RestResponse deleteUser(@PathVariable String username){
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("USER_NAME", username);
        UserEntity userEntity = loginMapper.selectOne(wrapper);
        if (userEntity!=null){
            loginMapper.delete(wrapper);
            return RestResponse.succuess("删除成功");
        }
        return RestResponse.fail("删除失败");
    }

    // @RequestMapping(value = "/people/updateUser",method = RequestMethod.POST)
    @ApiOperation("更新用户接口")
    @PostMapping("/people/updateUser")
    public RestResponse updateUser(UserEntity user){
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("USER_NAME", user.getUsername());
        int i = loginMapper.update(user, wrapper);

        if (i>0){
            return RestResponse.succuess("用户更新成功");
        }
        return RestResponse.fail("更新失败");
    }
    // @RequestMapping(value = "/people/select",method = RequestMethod.GET)
    @ApiOperation("查询用户接口")
    @Cacheable(value = "userList",keyGenerator = "keyGenerator")
    @GetMapping(value = {"/people/select/{currentPage}/{limit}","/people/select"})
    public RestResponse select(@PathVariable(value = "currentPage",required =false) Long currentPage,
                               @PathVariable(value = "limit",required =false) Long limit){

        if (currentPage==null) {
            currentPage=1L;
        }
        if (limit==null){
            limit=10L;
        }
        Page<UserEntity> userEntityPage = new Page<>(currentPage, limit);
        Page<UserEntity> page = loginMapper.selectPage(userEntityPage, null);

        return RestResponse.succuess(page);
    }
    /**
     * 用户管理 ----- 角色管理
     */

    // 新建角色
    @ApiOperation("新建角色")
    @PostMapping("people/addRole")
    public RestResponse addRole(RoleEntity role){

        RoleEntity roleExist = roleMapper.selectById(role.getRoleId());
        if (roleExist==null){
            int insert = roleMapper.insert(role);
            if (insert>0){
                return RestResponse.succuess("新增角色成功");
            }
        }else {
            return RestResponse.fail("角色已存在");
        }

        return RestResponse.fail("新增角色失败");
    }
    // 删除角色
    @ApiOperation("删除角色")
    @PostMapping("people/deleteRole/{roleId}")
    public RestResponse deleteRole(@PathVariable Integer roleId){

        int i = roleMapper.deleteById(roleId);
        if (i>0){
            return  RestResponse.succuess("删除成功");
        }
        return RestResponse.fail("删除失败");
    }
    // 角色编辑
    @ApiOperation("角色编辑")
    @PostMapping("people/updateRole")
    public RestResponse updateRole(RoleEntity role){
        int i = roleMapper.updateById(role);
        if (i>0){
            return RestResponse.succuess("角色编辑成功");
        }
        return  RestResponse.fail("角色编辑失败");
    }

    /**
     * 用户管理 ----- 权限管理
     */
    @ApiOperation("增加权限")
    @GetMapping("/permission/addRoleMenu")
    public RestResponse addRoleMenu(RoleMenuRelEntity roleMenuRelentity) {
        Integer integer = roleMenuRelMapper.selectCount(new QueryWrapper<RoleMenuRelEntity>().eq("MENU_ID", roleMenuRelentity.getMenuID()));
        if (integer >0) {
            return RestResponse.fail("插入失败，权限已存在");
        }else {
            int insert = roleMenuRelMapper.insert(roleMenuRelentity);
            if (insert>0){
                return RestResponse.succuess("插入成功！！");
            }
            return RestResponse.fail("插入失败！！");
        }
    }
    @ApiOperation("删除权限")
    @PostMapping("/permission/delRoleMenu/{roleID}")
    public RestResponse delRoleMenu(@PathVariable Integer roleID){
        QueryWrapper<RoleMenuRelEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("ROLE_ID",roleID);
        Integer integer = roleMenuRelMapper.selectCount(wrapper);
        if (integer==0){
            return RestResponse.fail("删除失败");

        }else {
            int i = roleMenuRelMapper.delete(wrapper);
            if (i>0) {
                return RestResponse.succuess("删除成功");
            }
            return RestResponse.fail("删除失败");
        }
    }
    @ApiOperation("修改权限")
    @PostMapping("/permission/updRoleMenu")
    public RestResponse updRoleMenu(RoleMenuRelEntity roleMenuRelentity){
        int i = roleMenuRelMapper.update(roleMenuRelentity,new UpdateWrapper<RoleMenuRelEntity>().eq("ROLE_ID",roleMenuRelentity.getRoleID()));
        if (i>0){
            return RestResponse.succuess("修改成功");
        }else {
            return RestResponse.fail("修改失败");
        }
    }

    @ApiOperation("查询权限")
    @PostMapping("/permission/selectRoleMenu")
    public RestResponse queRoleMenu(){
        List<RoleMenuRelEntity> list = roleMenuRelMapper.selectList(null);
        if (list!=null){
            return RestResponse.succuess("查询成功"+list);
        }else {
            return RestResponse.fail("查询失败");
        }

    }
    //权限分配
   @ApiOperation("修改权限分配")
    @PostMapping("/permission/disper/")
    public RestResponse disper(UserRoleRelEntity userRoleRelEntity){
        int i = userRoleRelMapper.update(userRoleRelEntity,new QueryWrapper<UserRoleRelEntity>().eq("USER_ID",userRoleRelEntity.getUserID()));
        if (i>0){
            return RestResponse.succuess("修改成功");
        }else {
            return RestResponse.fail("修改失败");
        }

    }
    @ApiOperation("创建权限分配")
    @GetMapping("/permission/addper/")
    public RestResponse addper(UserRoleRelEntity userRoleRelEntity) {
        Integer integer = userRoleRelMapper.selectCount(new QueryWrapper<UserRoleRelEntity>().eq("ROLE_ID", userRoleRelEntity.getUserID()));
        if (integer > 0) {
            return RestResponse.fail("创建失败，权限已分配");
        } else {
            int insert = userRoleRelMapper.insert(userRoleRelEntity);
            if (insert > 0) {
                return RestResponse.succuess("权限分配成功");
            }
                return RestResponse.fail("权限分配失败");
        }
    }
    @ApiOperation("删除权限分配")
    @PostMapping("/permission/delper/{userID}")
    public RestResponse delper(String userID){

        QueryWrapper<UserRoleRelEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("USER_ID",userID);
        Integer integer = userRoleRelMapper.selectCount(wrapper);
        if (integer==0){
            return RestResponse.fail("删除失败");
        }else {
            int i = userRoleRelMapper.delete(wrapper);
            if (i>0){
                return RestResponse.succuess("删除成功！！");
            }
            return RestResponse.fail("删除失败");
        }

    }
    @ApiOperation("查询权限分配")
    @PostMapping("/permission/queper/")
    public RestResponse queper(){
        List<UserRoleRelEntity> i = userRoleRelMapper.selectList(null);
        if (i!=null){
            return RestResponse.succuess("查询成功"+i);
        }else {
            return RestResponse.fail("查询失败");
        }

    }
}
