package com.capgemini.demo.controller.BitBucketController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.capgemini.demo.dto.commit.GetCommitsDto;
import com.capgemini.demo.service.GetBranchService;
import com.capgemini.demo.tools.JsonUtils.JsonUtils;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@Api(tags = "获取Branch信息")
@RestController
@RequestMapping("/api/branch")
public class GetBranchController {
    @Autowired
    GetBranchService getBranchService;
    @PostMapping("/all")
    @ApiOperation(value = "更新branch")
    public RestResponse allBranch(GetCommitsDto commitsDto) throws ParseException {
        boolean branch = getBranchService.getBranch(commitsDto);
        if (branch){
            return RestResponse.succuess("操作成功");
        }
        return RestResponse.fail("操作失败");


    }
}
