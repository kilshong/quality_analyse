package com.capgemini.demo.mapper.login;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.permission.UserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LoginMapper extends BaseMapper<UserEntity> {
}
