package com.capgemini.demo.mapper.BitBucketMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.bitbucket.BitBucketEntity;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface BitBucketMapper extends BaseMapper<BitBucketEntity> {
}
