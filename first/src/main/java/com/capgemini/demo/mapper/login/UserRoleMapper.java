package com.capgemini.demo.mapper.login;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.login.UserRoleRelEntity;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleMapper extends BaseMapper<UserRoleRelEntity> {
}
