package com.capgemini.demo.mapper.BitBucketMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.bitbucket.BranchEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BitBranchMapper extends BaseMapper<BranchEntity> {
}
