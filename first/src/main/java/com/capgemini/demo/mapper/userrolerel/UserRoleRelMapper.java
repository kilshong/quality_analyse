package com.capgemini.demo.mapper.userrolerel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.permission.UserRoleRelEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleRelMapper extends BaseMapper<UserRoleRelEntity> {
}
