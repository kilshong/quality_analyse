package com.capgemini.demo.mapper.BitBucketMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.bitbucket.BitBucketEntity;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface BitMessageMapper extends BaseMapper<BitBucketEntity> {
    //通过不同条件来查询
    @Select("<script>" +
            "select U_ID,REPOSITORY_NAME,REPOSITORY_TYPE,REPOSITORY_FULL_NAME,AUTHOR_RAW,AUTHOR_TYPE,BITBUCKET_TYPE,OP_TIME,MESSAGE,HASH from TB_BITBUCKET" +
            "<where>" +
            "<if test='REPOSITORY_NAME !=null '>" +
            "REPOSITORY_NAME=#{REPOSITORY_NAME}" +
            "</if>" +
            "<if test='REPOSITORY_FULL_NAME !=null'>" +
            "and REPOSITORY_FULL_NAME=#{REPOSITORY_FULL_NAME}" +
            "</if>" +
            "<if test='AUTHOR_RAW !=null'>" +
            "and AUTHOR_RAW like concat ('%',concat(#{AUTHOR_RAW},'%'))" +
            "</if>" +
            "<if test='BITBUCKET_TYPE !=null'>" +
            "and BITBUCKET_TYPE like concat('%',concat(#{BITBUCKET_TYPE},'%'))" +
            "</if>" +
            "<if test='OP_TIME !=null'>" +
            "and OP_TIME like concat('%',concat(#{OP_TIME},'%'))" +
            "</if>" +
            "</where>" +
            "</script>")
    @Results({
            @Result(column = "OP_TIME", property = "optrationTime", jdbcType = JdbcType.VARCHAR)})
    List<BitBucketEntity> selectAll(@Param("REPOSITORY_NAME") String REPOSITORY_NAME,
                                    @Param("REPOSITORY_FULL_NAME") String REPOSITORY_FULL_NAME,
                                    @Param("AUTHOR_RAW") String AUTHOR_RAW,
                                    @Param("BITBUCKET_TYPE") String BITBUCKET_TYPE,
                                    @Param("OP_TIME") String OP_TIME);

    //查询谁，提交的次数
    @Select("select count(*) from TB_BITBUCKET where AUTHOR_RAW like concat ('%',concat(#{AUTHOR_RAW},'%'))" +
            "and MESSAGE=#{MESSAGE}")
    int count(@Param("AUTHOR_RAW") String AUTHOR_RAW,
              @Param("MESSAGE") String MESSAGE);


}
