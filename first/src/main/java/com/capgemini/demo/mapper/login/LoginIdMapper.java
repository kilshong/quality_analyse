package com.capgemini.demo.mapper.login;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.login.LoginIdEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LoginIdMapper extends BaseMapper<LoginIdEntity> {
}
