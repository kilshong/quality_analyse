package com.capgemini.demo.mapper.login;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.capgemini.demo.entity.permission.RoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: L
 * @Date: 2021/10/25 14:27
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleEntity> {
}
