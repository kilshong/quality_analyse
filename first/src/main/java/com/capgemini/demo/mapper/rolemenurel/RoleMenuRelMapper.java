package com.capgemini.demo.mapper.rolemenurel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.capgemini.demo.entity.permission.RoleMenuRelEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleMenuRelMapper extends BaseMapper<RoleMenuRelEntity> {

}
