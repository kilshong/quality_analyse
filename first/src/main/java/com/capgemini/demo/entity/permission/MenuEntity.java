package com.capgemini.demo.entity.permission;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 用户权限
 */
@Data
@TableName("TB_USER_MENU")
public class MenuEntity {
    @TableId(type = IdType.ASSIGN_ID)
    @TableField("MENU_ID")
    private String menuId;
    @TableField("MENU_NAME")
    private String menuName;
    @TableField("MENU_TYPE")
    private String menuType;
    @TableField("PARENT_ID")
    private String parentId;
    @TableField("STATUS")
    private Integer status;
    @TableField("URL")
    private String url;
    @TableField("COUNT_NUM")
    private Integer countNum;
    @TableField("CREATE_TIME")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @TableField("MODIFY_TIME")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifyTime;
}
