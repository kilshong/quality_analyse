package com.capgemini.demo.entity.login;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("TB_USER_LOGIN_ROLE_ROLE")
public class LoginIdEntity {
    @TableField("USERNAME")
    private String username;
    @TableField("LOGINID")
    private String loginId;
    @TableField("ROLE")
    private String role;
}
