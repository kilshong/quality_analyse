package com.capgemini.demo.entity.permission;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("TB_USER_ROLE_REL")
public class UserRoleRelEntity {
    @TableField("USER_ID")
    private String userID;
    @TableField("ROLE_ID")
    private Integer roleID;
}
