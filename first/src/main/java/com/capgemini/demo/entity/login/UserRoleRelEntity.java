package com.capgemini.demo.entity.login;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("TB_USER_ROLE_REL")
public class UserRoleRelEntity {
    @TableField("USER_ID")
    private String userId;
    @TableField("ROLE_ID")
    private Integer roleId;
}
