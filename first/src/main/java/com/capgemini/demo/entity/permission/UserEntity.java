package com.capgemini.demo.entity.permission;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@TableName("TB_USER")
public class UserEntity {
    @TableField("USER_ID")
    private String uId;
    @TableField("USER_NAME")
    private String username;
    @TableField("PASSWORD")
    private String password;
    @TableField(value = "CREATE_TIME",fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(value = "MODIFY_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date modifyTime;
    @TableField("STATUS")
    private Integer status;
    @TableField(value = "LAST_LOGIN_TIME",fill = FieldFill.UPDATE)
    private Date lastLoginTime;
}
