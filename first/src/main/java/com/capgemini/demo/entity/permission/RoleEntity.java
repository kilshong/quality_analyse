package com.capgemini.demo.entity.permission;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("TB_USER_ROLE")
public class RoleEntity {
    @TableId(type = IdType.ASSIGN_ID)
    @TableField("ROLE_ID")
    private Integer roleId;
    @TableField("ROLE_NAME")
    private String roleName;
}
