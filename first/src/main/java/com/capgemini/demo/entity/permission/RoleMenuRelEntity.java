package com.capgemini.demo.entity.permission;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("TB_USER_ROLE_MENU_REL")
public class RoleMenuRelEntity {
    @TableField("ROLE_ID")
    private Integer roleID;
    @TableField("MENU_ID")
    private String menuID;
}
