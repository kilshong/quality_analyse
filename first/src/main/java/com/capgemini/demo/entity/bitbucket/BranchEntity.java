package com.capgemini.demo.entity.bitbucket;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("TB_BRANCH")
public class BranchEntity {
    @TableField("NAME")
    private String name;
    @TableField("TYPE")
    private String type;
    @TableField("OP_TIME")
    private String opTime;
    @TableField("HASH")
    private String hash;
}
