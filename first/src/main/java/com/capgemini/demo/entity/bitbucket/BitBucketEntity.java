package com.capgemini.demo.entity.bitbucket;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author kils
 */
@Data
@TableName("TB_BITBUCKET")
public class BitBucketEntity {
    @TableId(type = IdType.ASSIGN_ID)
    @TableField("U_ID")
    private String uId;
    @TableField("REPOSITORY_NAME")
    private String repositoryName;
    @TableField("REPOSITORY_TYPE")
    private String repositoryType;
    @TableField("REPOSITORY_FULL_NAME")
    private String repositoryFullName;
    @TableField("AUTHOR_RAW")
    private String authorRaw;
    @TableField("AUTHOR_TYPE")
    private String authorType;
    @TableField("BITBUCKET_TYPE")
    private String bitBucketType;
    @TableField("OP_TIME")
    // @JsonFormat(pattern = "yyyy:MM:dd HH:mm:ss",timezone = "GMT+8")
    private String optrationTime;
    @TableField("MESSAGE")
    private String message;
    @TableField("HASH")
    private String hash;
}
