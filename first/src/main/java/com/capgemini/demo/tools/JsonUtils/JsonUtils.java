package com.capgemini.demo.tools.JsonUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * JSON转换工具类
 */
public class JsonUtils {
    // private static final String URL = "https://api.bitbucket.org";
    private static final String URL = "https://devopscn.capgemini.com";
    public static JSONObject loadJson(String api) {
        StringBuilder json = new StringBuilder();
        try {
            URL urlObject = new URL(URL+api);
            URLConnection uc = urlObject.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), "utf-8"));
            String inputLine = null;
            while ((inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return JSON.parseObject(json.toString());
    }
}
