package com.capgemini.demo.tools;

import io.swagger.models.auth.In;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: L
 * @Date: 2021/10/28 17:23
 */

public class CommitUtils {

    /*
     *  host:服务器ip ex:https://api.bitbucket.org
     *  workspace : 工作区
     *  repo_slug : 仓库名
     *  hash: 每次提交的唯一标识
     *  return : 删除的行数  增加的行数
     * */
    public static List<Integer> getCount( String workspace, String repo_slug, String hash) {
        BufferedReader in = null;
        InputStream inputStream = null;
        InputStreamReader reader = null;
        StringBuilder url = new StringBuilder();
        List<Integer> result = new ArrayList<>();
        url.append("https://api.bitbucket.org/2.0/repositories/").append(workspace).append("/").append(repo_slug).append("/").append("diff/").append(hash);
        int addCount = 0;
        int deleteCount = 0;
        int sum = 0;
        try {
            URLConnection connection = new URL(url.toString()).openConnection();
            inputStream = connection.getInputStream();
            reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            in = new BufferedReader(reader);
            String contentLine = in.readLine();
            while (contentLine != null) {
                if (!contentLine.contains("+++") && !contentLine.contains("---")) {
                    if (contentLine.charAt(0) == '+') {
                        addCount++;
                    }
                    if (contentLine.charAt(0) == '-') {
                        deleteCount++;
                    }
                } else {
                    String substring = contentLine.substring(0, 3);
                    if (!substring.equals("+++") && !substring.equals("---")) {
                        if (contentLine.charAt(0) == '+') {
                            addCount++;
                        }
                        if (contentLine.charAt(0) == '-') {
                            deleteCount++;
                        }
                    }

                }
                contentLine = in.readLine();
            }

            result.add(deleteCount);
            result.add(addCount);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
