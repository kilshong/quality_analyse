package com.capgemini.demo.Scheduled;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class GetDateCommitsSheduled {
    /**
     * 通过接口获取所有的commits
     */
    @Scheduled
    public void getDateCommits(){

    }
    /**
     * 通过api获取所有仓库信息
     */
    public void getDateRepository(){

    }
}
