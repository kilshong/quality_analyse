package com.capgemini.demo.dto.commit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SummaryDto {
    private String raw;
    private String marke;
    private String html;
    private String type;
}
