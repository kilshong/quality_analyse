package com.capgemini.demo.dto.login;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("用户名和密码的包装dto")
public class LoginDto {
    private String username;
    private String password;
}
