package com.capgemini.demo.dto.commit;

import lombok.Data;

@Data
public class GetCommitsDto {
    /**
     * 用户
     */
    private String user;
    /**
     * 仓库
     */
    private String repository;
}
