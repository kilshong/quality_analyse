package com.capgemini.demo.service.servceImpl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.capgemini.demo.dto.commit.GetCommitsDto;
import com.capgemini.demo.entity.bitbucket.BranchEntity;
import com.capgemini.demo.mapper.BitBucketMapper.BitBranchMapper;
import com.capgemini.demo.service.GetBranchService;
import com.capgemini.demo.tools.JsonUtils.JsonUtils;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class GetBranchServiceImpl implements GetBranchService {
    @Autowired
    BitBranchMapper bitBranchMapper;
    @Override
    public boolean getBranch(GetCommitsDto commitsDto) throws ParseException {
        if (!commitsDto.equals(null)) {
            String workplace = commitsDto.getUser();
            String repository = commitsDto.getRepository();
            JSONObject jsonObject = JsonUtils.loadJson("/2.0/repositories/" + workplace + "/" + repository + "/refs/branches");
            JSONArray values = (JSONArray) jsonObject.get("values");
            System.out.println(values);
            for (Object value : values) {
                System.out.println(value);
                JSONObject json = (JSONObject) value;
                String name = (String)json.get("name");
                String type = (String)json.get("type");
                JSONObject target = (JSONObject) json.get("target");
                String date = (String) target.get("date");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = simpleDateFormat.parse(date);
                String opTime = dateFormat.format(d);
                String hash = (String) target.get("hash");
                System.out.println(hash);
                BranchEntity branchEntity = new BranchEntity();
                branchEntity.setName(name);
                branchEntity.setType(type);
                branchEntity.setHash(hash);
                branchEntity.setOpTime(opTime);
                bitBranchMapper.insert(branchEntity);
            }
            return true;
        } else {
            return false;
        }
    }
}
