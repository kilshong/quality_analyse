package com.capgemini.demo.service.servceImpl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.capgemini.demo.entity.bitbucket.BitBucketEntity;
import com.capgemini.demo.mapper.BitBucketMapper.BitBucketMapper;
import com.capgemini.demo.service.GetCommitsService;
import com.capgemini.demo.tools.JsonUtils.JsonUtils;
import com.capgemini.demo.tools.RestResponse.RestResponse;
import lombok.SneakyThrows;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class GetCommitsServiceImpl implements GetCommitsService {

    String date = null;
    String message = null;
    String type = null;
    String authortype = null;
    Object o1 = null;
    String fullNamePerson = null;

    @Autowired
    private BitBucketMapper bitBucketMapper;

    /**
     * 实现拆开JsonObject数据
     */
    @SneakyThrows
    @Override
    public boolean addCommits(JSONObject jsonObject) {
        Map<String, Object> map = new HashMap();
        Iterator<Map.Entry<String, Object>> it = jsonObject.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            if (!(entry.getValue() instanceof Integer)) {
                System.out.println("key= " + entry.getKey() + " and value= " + entry.getValue());
                JSONArray jsonArray = (JSONArray) entry.getValue();
                Iterator<Object> iterator = jsonArray.iterator();
                while (iterator.hasNext()) {
                    o1 = iterator.next();
                    System.out.println(o1);
                    map = JSON.parseObject(o1.toString(), new TypeReference<Map<String, Object>>() {
                    });
                    Map<String, Object> map2 = gethashMap(map.get("repository"));
                    // 仓库的类型
                    String uuid = (String) map2.get("uuid");
                    String hash = (String) map.get("hash");
                    BitBucketEntity bitBucketEntity = bitBucketMapper.selectOne(new QueryWrapper<BitBucketEntity>().eq("HASH", hash));
                    BitBucketEntity bitBucket = new BitBucketEntity();
                    map2.get("full_name");// 仓库的全名
                    map2.get("name"); // 仓库的名字
                    map2.get("type");
                    System.out.println("==================发布时间===================");
                    date = (String) map.get("date"); // 发布时间
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d = simpleDateFormat.parse(date);
                    String opTime = dateFormat.format(d);
                    System.out.println(date);
                    System.out.println("=================发布时间====================");
                    message = (String) map.get("message"); // 信息
                    type = (String) map.get("type"); // commits
                    Map<String, Object> map1 = gethashMap(map.get("author"));
                    fullNamePerson = (String) map1.get("raw"); // 包括邮箱的名字
                    authortype = (String) map1.get("type"); // 作者类型
                    // UUID
                    bitBucket.setUId(uuid);
                    bitBucket.setRepositoryFullName((String) map2.get("full_name"));
                    bitBucket.setRepositoryName((String) map2.get("name"));
                    bitBucket.setRepositoryType((String) map2.get("type"));
                    bitBucket.setAuthorRaw(fullNamePerson);
                    bitBucket.setAuthorType(authortype);
                    bitBucket.setOptrationTime(opTime);
                    bitBucket.setBitBucketType(message);

                    bitBucket.setMessage(type);
                    bitBucket.setHash(hash);
                    int insert = bitBucketMapper.insert(bitBucket);
                }

            }

        }
        System.out.println("==========" + map + "================");
        System.out.println(map.get("author"));
        return false;
    }


    @Override
    public int getNumCommits(String author, String repository) {
        // List<BitBucketEntity> author_raw = bitBucketMapper.selectList(new QueryWrapper<BitBucketEntity>().like("AUTHOR_RAW", author + "%").eq("REPOSITORY_NAME",repository));
        List<BitBucketEntity> author_raw = bitBucketMapper.selectList(new QueryWrapper<BitBucketEntity>().and(i -> i.like("AUTHOR_RAW", author + "% ").eq("REPOSITORY_NAME", repository)));
        int size = author_raw.size();
        return size;
    }

    public static Map<String, Object> gethashMap(Object o1) {
        Map<String, Object> map = JSON.parseObject(o1.toString(), new TypeReference<Map<String, Object>>() {
        });
        return map;
    }
}
