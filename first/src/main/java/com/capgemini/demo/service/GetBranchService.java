package com.capgemini.demo.service;

import com.capgemini.demo.dto.commit.GetCommitsDto;

import java.text.ParseException;

public interface GetBranchService {
    boolean getBranch(GetCommitsDto commitsDto) throws ParseException;
}
