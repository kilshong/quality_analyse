package com.capgemini.demo.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;


public interface GetCommitsService {
    /**
     * 更新数据
     * @param jsonObject
     * @return
     */
    boolean addCommits(JSONObject jsonObject);
    /**
     * 查询作者提交的数量数量
     */
    int getNumCommits(String author,String repository);
    /**
     * 根据作者仓库查询所有
     */
}
