package com.capgemini.demo.config;

import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 设置Permission
 */
//@Configuration
public class PermissonAnalysisConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SaRouteInterceptor((req, res, handler) -> {
            // 根据路由划分模块，不同模块不同鉴权
            SaRouter.match("/api/user/permission/**", r -> StpUtil.checkRoleOr("Admin"));
            SaRouter.match("/api/user/role/**", r -> StpUtil.checkRoleOr("Admin"));
            SaRouter.match("/api/user/people/**", "/api/user/people/select", r -> StpUtil.checkRoleOr("Admin"));
        })).addPathPatterns("/api/**");

    }
}
